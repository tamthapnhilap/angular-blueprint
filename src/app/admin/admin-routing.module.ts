import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminNotfoundComponent } from './admin-notfound/admin-notfound.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';

const routes: Routes = [
  {
    path: '',
    component: AdminDashboardComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: AdminHomeComponent
      },
      {
        path: '**',
        component: AdminNotfoundComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
