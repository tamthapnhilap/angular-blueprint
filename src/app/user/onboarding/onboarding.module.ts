import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OnboardingRoutingModule } from './onboarding-routing.module';
import { OnboardingComponent } from './onboarding.component';
import { LoginComponent } from './login/login.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationComponent } from './notification/notification.component';
import { FormComponent } from './login/form/form.component';
import { YtplaylistComponent } from './welcome/ytplaylist/ytplaylist.component';
import { YtsongComponent } from './welcome/ytplaylist/ytsong/ytsong.component';

@NgModule({
  declarations: [
    OnboardingComponent,
    LoginComponent,
    WelcomeComponent,
    NotificationComponent,
    FormComponent,
    YtplaylistComponent,
    YtsongComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    OnboardingRoutingModule
  ]
})
export class OnboardingModule { }
