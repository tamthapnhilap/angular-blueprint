import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YtplaylistComponent } from './ytplaylist.component';

describe('YtplaylistComponent', () => {
  let component: YtplaylistComponent;
  let fixture: ComponentFixture<YtplaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtplaylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtplaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
