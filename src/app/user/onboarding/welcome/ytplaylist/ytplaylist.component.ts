import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observer, Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { EventEmitter } from 'events';

interface YTSong {
  name: string;
  thumbnail: string;
  video: string;
  owner: string;
  link: string;
  views: number;
  likes: number;
  dislikes: number;
}

@Component({
  selector: 'app-welcome-ytplaylist',
  templateUrl: './ytplaylist.component.html',
  styleUrls: ['./ytplaylist.component.scss']
})
export class YtplaylistComponent implements OnInit {

  // Sample Endpoint : https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PLeMP6FfkJmGBXDV-j22oben8B90X-l-K-&key={YOUR_API_KEY}
  // https://developers.google.com/youtube/v3/docs/playlistItems/list
  // https://developers.google.com/apis-explorer/#p/youtube/v3/youtube.playlistItems.list?part=snippet%252CcontentDetails&maxResults=50&playlistId=PLeMP6FfkJmGBXDV-j22oben8B90X-l-K-&_h=6&

  sectionTitle: string = "Youtube Playlists";
  totalSongs: number;
  ytsongs: Array<any> = [];
  currentSong: YTSong;

  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.fetchYTSong();
  }

  fetchYTSong(filter = { s: "" }) {
    let observable: Observable<any> = this.http.get('assets/mocks/ytplaylist.json');
    observable.subscribe(
      (data: any) => { console.log(data); this.reduceSongs(data.items, filter); }, //next
      (error: any) => { console.log(error) }, //error
      () => { console.log('complete') }, //complete
    )
  }

  reduceSongs(songs: Array<any>, filter = { s: "" }) {
    this.ytsongs = songs.map((song: any, index: number) => {
      let snippet = song.snippet;
      return this.createYTSong(
        snippet.title,
        this.sanitizer.bypassSecurityTrustResourceUrl(snippet.thumbnails.medium.url),
        this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/embed/${snippet.resourceId.videoId}?autoplay=1`),
        snippet.description,
        this.sanitizer.bypassSecurityTrustResourceUrl(`https://www.youtube.com/watch?v=${snippet.resourceId.videoId}`),
        0,
        0,
        0
      );
    }).filter((song: YTSong, index: number) => {
      if (filter["s"].length == 0) return true;
      return song.name.match(new RegExp(filter["s"], "i"));
    });
  }

  createYTSong(
    name, thumbnail, video, owner,
    link, views, likes,
    dislikes
  ): YTSong {
    return {
      name: name,
      thumbnail: thumbnail,
      video: video,
      owner: owner,
      link: link,
      views: views,
      likes: likes,
      dislikes: dislikes
    }
  }

  playSong(song: YTSong) {
    this.currentSong = song;
  }

  searchSong(searchText: string) {
    this.fetchYTSong({ s: searchText });
  }
}
