import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YtsongComponent } from './ytsong.component';

describe('YtsongComponent', () => {
  let component: YtsongComponent;
  let fixture: ComponentFixture<YtsongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtsongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtsongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
