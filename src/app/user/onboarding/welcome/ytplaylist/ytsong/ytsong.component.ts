import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

interface YTSong {
  name: string;
  thumbnail: string;
  video: string;
  owner: string;
  link: string;
  views: number;
  likes: number;
  dislikes: number;
}

@Component({
  selector: 'app-ytsong-item',
  templateUrl: './ytsong.component.html',
  styleUrls: ['./ytsong.component.scss']
})
export class YtsongComponent implements OnInit {

  @Input() id: number;
  @Input() ytsong: YTSong;
  @Output() playSong = new EventEmitter<YTSong>();

  isDisplayed: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  showToggle() {
    this.isDisplayed = !this.isDisplayed;
  }

  play(song: YTSong) {
    this.playSong.emit(song)
  }

}
