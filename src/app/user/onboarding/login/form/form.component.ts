import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-login',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Input() loginType: string;
  @Output() doLogin = new EventEmitter<any>();

  loginForm = this.fb.group({
    login: ['', Validators.required],
    password: ['', Validators.required],
    remember: ['']
  })

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.loginForm.value);
    this.loginForm.disable();
    // login successfull
    this.doLogin.emit({
      success: true,
      message: "Welcome to Angular blueprint app!"
    });
  }
}
