import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  notification: string = '';
  loginType: boolean = true;// true: email, false:mobile
  // #formControl
  // email = new FormControl('');
  // password = new FormControl('');
  // remember = new FormControl('');
  // #group
  // loginForm = new FormGroup({
  //   email: new FormControl(''),
  //   password: new FormControl(''),
  //   remember: new FormControl(''),
  // });
  // #validation


  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
  }

  convertLoginType() {
    return this.loginType ? "email" : "mobile"
  }

  switchLoginType() {
    this.loginType = !this.loginType;
  }

  updateNotification(notification) {
    this.notification = notification;
  }

  onDoLogin(data: any) {
    console.log(data);
    this.updateNotification(data.message);
    // const loginData = this.loginForm.value;
    // console.log(loginData);
    // this.notification = "OK";
  }

}
