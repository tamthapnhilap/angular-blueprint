import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { UserNotfoundComponent } from './user-notfound/user-notfound.component';
import { UserWelcomeComponent } from './user-welcome/user-welcome.component';

@NgModule({
  declarations: [UserComponent, UserNotfoundComponent, UserWelcomeComponent],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
