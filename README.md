# AngularBlueprint

Getting start

```
ng new --help
ng new {APP_DIR} --skip-install --styles=scss
cd {APP_DIR}
yarn install
```

## Sample Domain Feature Module

- user/onboarding
    - user/onboarding/login ( login page )
    - user/onboarding/welcome ( default or 404 )

```bash
ng generate module user/onboarding/onboarding-routing --flat --module=user/onboarding --routing=true
ng generate component user/onboarding/login --module=user/onboarding
ng generate component user/onboarding/welcome --module=user/onboarding
```

## Sample use external css framework

```bash
yarn add bulma
```

**styles.scss**

```scss
/* You can add global styles to this file, and also import other style files */
@charset "utf-8";

// Customization

// 1. Import the initial variables
@import "./node_modules/bulma/sass/utilities/initial-variables";

// 2. Set your own initial variables
// Update the blue shade, used for links
$blue: #06bcef;
// Add pink and its invert
$pink: #ff8080;
$pink-invert: #fff;
// Update the sans-serif font family
$family-sans-serif: "Helvetica", "Arial", sans-serif;

// 3. Set the derived variables
// Use the new pink as the primary color
$primary: $pink;
$primary-invert: $pink-invert;

// 4. Import the rest of Bulma
@import "./node_modules/bulma/bulma";
```

**Directly Way : angular.json**


```json
"styles": [
    "node_modules/bulma/css/bulma.css",
    "src/styles.scss"
],
```

